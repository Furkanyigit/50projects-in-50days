const fill = document.querySelector('.fill')
const empties = document.querySelectorAll('.empty')

fill.addEventListener('dragstart', dragStart)
fill.addEventListener('dragend', dragEnd)

for (const empty of empties) {
    empty.addEventListener('dragover', dragOver)
    empty.addEventListener('dragenter', dragEnter)
    empty.addEventListener('dragleave', dragLeave)
    empty.addEventListener('drop', dragDrop)
}

function dragStart() {
    this.ClassName += 'hold'
    setTimeout(() => this.cllasName = 'invisible', 0)

}

function dragEnd() {
    this.ClassName = 'fill'
}

function dragOver(e) {
    e.preventDefault()
}

function dragEnter(e) {
    e.preventDefault()
    this.ClassName = 'empty'

}

function dragLeave() {
    this.ClassName += ' hovered'
}

function dragDrop() {
    this.ClassName = 'empty'
    this.append(fill)
}